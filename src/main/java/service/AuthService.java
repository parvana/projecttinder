package service;

import classes.User;
import dao.UserDao;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AuthService {

    UserDao dao = new UserDao();

    public void save(User data) throws SQLException {
        dao.save(data);
    }

    public boolean check(String email, String paswd) throws SQLException {
        List<User> users = dao.get(p -> p.getEmail().equals(email) && p.getPassword().equals(paswd));
        return !users.isEmpty();
    }

    public User getById(int id) throws SQLException {
        List<User> user=dao.get(p->p.getId()==id);
        return user.get(0);
    }
    public User getByEmail(String email) throws SQLException {
        List<User> user=dao.get(p->p.getEmail().equals(email));
        return user.get(0);
    }

    public List<User> getALL() throws SQLException {
        return dao.getAll();
    }
}


