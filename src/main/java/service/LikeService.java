package service;
import classes.Like;
import classes.User;
import dao.LikeDao;
import java.sql.SQLException;
import java.util.List;

public class LikeService {
    LikeDao likeDao=new LikeDao();


    public void save(Like data) throws SQLException {
        likeDao.save(data);
    }

    public List<Like> getById(int fromwho) throws SQLException {
        List<Like> user=likeDao.get(p->p.getFromWhom()==fromwho);
        return user;
    }
    public boolean beingLiked(int fromwho,int towhom) throws SQLException {

        List<Like> likedusers=likeDao.get(p->p.getFromWhom()==fromwho && p.getToWho()==towhom );
        if(likedusers.size()==0) return false;
        else return true;

    }
    public List<Like> getALL() throws SQLException {
        return likeDao.getAll();
    }

}
