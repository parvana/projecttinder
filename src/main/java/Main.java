import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import service.AuthService;
import service.LikeService;
import service.MesService;
import servlet.*;

public class Main {
    public static void main(String[] args) throws Exception {
        TemplateEngine engine = TemplateEngine.folder("./templates");
        AuthService authService=new AuthService();
        LikeService likeService=new LikeService();
        MesService mesService=new MesService();
        ServletContextHandler handler=new ServletContextHandler();
        handler.addServlet(new ServletHolder(new LoginServlet()),"/login/*");
        handler.addServlet(new ServletHolder(new UsersServlet(engine,authService,likeService)),"/users/*");
        handler.addServlet(new ServletHolder(new LikeServlet(engine,likeService,authService)),"/like/*");
        handler.addServlet(new ServletHolder(new AllTheFiles()),"/static/*");
        handler.addServlet(new ServletHolder(new RegistrationServlet(authService)),"/registr/*");
        handler.addServlet(new ServletHolder(new LogoutServlet()),"/logout/*");
        handler.addServlet(new ServletHolder(new MessageServlet(engine,mesService)),"/chat/*");
        int port;
        try {
            port = Integer.parseInt(System.getenv("PORT"));
        } catch (NumberFormatException ex) {
            port = 5000;
        }
        Server server = new Server(port);

        server.setHandler(handler);
        server.start();
        server.join();





    }
}
