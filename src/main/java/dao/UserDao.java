package dao;

import classes.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class UserDao implements Dao<User> {
    Connection conn = DbConnection.getConnection();

    @Override
    public void save(User user) throws SQLException {
        String query = "INSERT INTO users (name,surname,occupation,email,image,password) values(?,?,?,?,?,?)";
        PreparedStatement ps = conn.prepareStatement(query);
        ps.setString(1, user.getName());
        ps.setString(2, user.getSurname());
        ps.setString(3,user.getOccupation());
        ps.setString(4, user.getEmail());
        ps.setString(5,user.getImageurl());
        ps.setString(6, user.getPassword());
        ps.execute();

    }

    @Override
    public void delete(int id) {

    }

    @Override
    public List<User> get(Predicate<User> p) throws SQLException {
        List<User> list = new ArrayList<>();
        String query = "SELECT * FROM users";
        PreparedStatement ps = conn.prepareStatement(query);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            User user = new User(rs.getInt("id"),rs.getString("name"), rs.getString("surname"), rs.getString("email"),
                    rs.getString("password"),rs.getString("occupation"),rs.getString("image"));

            list.add(user);
        }
        return list.stream().filter(p).collect(Collectors.toList());
    }
    @Override
    public void update(int id) {

    }

    @Override
    public List<User> getAll() throws SQLException {
        List<User> list = new ArrayList<>();
        String query = "SELECT * FROM users";
        PreparedStatement ps = conn.prepareStatement(query);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            User user = new User(rs.getInt("id"),rs.getString("name"), rs.getString("surname"), rs.getString("email"),
                    rs.getString("password"),rs.getString("occupation"),rs.getString("image"));

            list.add(user);
        }
        return list;
    }


}
