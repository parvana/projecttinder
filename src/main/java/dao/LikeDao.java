package dao;
import classes.Like;
import classes.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class LikeDao implements Dao<Like> {
    Connection conn = DbConnection.getConnection();
    @Override
    public void save(Like data) throws SQLException {
        String query = "INSERT INTO userlikes (fromwho,towhom) values(?,?)";
        PreparedStatement ps = conn.prepareStatement(query);
        ps.setInt(1, data.getFromWhom());
        ps.setInt(2, data.getToWho());
        ps.execute();

    }

    @Override
    public void delete(int id) {

    }

    @Override
    public List<Like> get(Predicate<Like> p) throws SQLException {
        List<Like> list = new ArrayList<>();
        String query = "SELECT * FROM userlikes";
        PreparedStatement ps = conn.prepareStatement(query);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            Like like = new Like(rs.getInt("id"),rs.getInt("fromwho"),rs.getInt("towhom"));

            list.add(like);
        }
        return list.stream().filter(p).collect(Collectors.toList());
    }

    @Override
    public void update(int id) {

    }

    @Override
    public List<Like> getAll() throws SQLException {
        List<Like> list = new ArrayList<>();
        String query = "SELECT * FROM userlikes";
        PreparedStatement ps = conn.prepareStatement(query);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            Like like = new Like(rs.getInt("id"),rs.getInt("fromwho"),rs.getInt("towhom"));

            list.add(like);
        }
        return list;
    }
}
