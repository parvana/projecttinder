package dao;

import classes.Messages;
import classes.User;
//import sun.plugin2.message.Message;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class MessageDao implements Dao<Messages> {
    Connection conn = DbConnection.getConnection();
    @Override
    public void save(Messages data) throws SQLException {
        String query = "INSERT INTO usermessages (sender,receiver,message) values(?,?,?)";
        PreparedStatement ps = conn.prepareStatement(query);
        ps.setInt(1,data.getSender());
        ps.setInt(2,data.getReceiver());
        ps.setString(3,data.getMessage());
        ps.execute();
    }

    @Override
    public void delete(int id) {

    }

    @Override
    public List<Messages> get(Predicate<Messages> p) throws SQLException {
        List<Messages> list = new ArrayList<>();
        String query = "SELECT * FROM usermessages";
        PreparedStatement ps = conn.prepareStatement(query);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            Messages message = new Messages(rs.getInt("id"),rs.getInt("sender"),
                    rs.getInt("receiver"),rs.getString("message"));

            list.add(message);
        }
        return list.stream().filter(p).collect(Collectors.toList());
    }

    @Override
    public void update(int id) {

    }

    @Override
    public List<Messages> getAll() throws SQLException {
       return null;
    }
}
