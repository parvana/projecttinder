package dao;

import java.sql.SQLException;
import java.util.List;
import java.util.function.Predicate;

public interface Dao<T> {
    void save(T data) throws SQLException;
    void delete(int id);
    List<T> get(Predicate<T> p) throws SQLException;
    void update(int id);
    List<T> getAll() throws SQLException;

}
