package servlet;
import classes.Messages;
import service.AuthService;
import service.MesService;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MessageServlet extends HttpServlet {
    private TemplateEngine templateEngine;
    private MesService mesService;
    private int sender=-1;
    private int receiver=0;
    private String message=null;
    private  HashMap<String,Object> map;
    private AuthService authService;

    public MessageServlet(TemplateEngine templateEngine,MesService mesService) {
        this.templateEngine=templateEngine;
        this.mesService = mesService;
        map=new HashMap<>();
        authService=new AuthService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Cookie[] cookies=req.getCookies();
        if(cookies!=null){
            for(Cookie c:cookies){
                if(c.getName().equals("%likes%")) {
                    sender=Integer.parseInt(c.getValue());
                }
            }
        }
        receiver=Integer.parseInt(req.getPathInfo().substring(1));
        System.out.println(receiver);
        List<Messages> list=new ArrayList<>();
        try {
            list=mesService.findmessages(sender,receiver);
            map.put("name",authService.getById(receiver));
            map.put("name2",authService.getById(sender));
            map.put("messagelist",list);
            System.out.println(list);
            templateEngine.render("chat.ftl",map,resp);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Cookie[] cookies=req.getCookies();
        if(cookies!=null){
            for(Cookie c:cookies){
                if(c.getName().equals("%likes%")) {
                    sender=Integer.parseInt(c.getValue());
                }
            }
        }
        message=req.getParameter("message");
            Messages data = new Messages(sender,receiver,message);
            try {
                mesService.save(data);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        try {
            List<Messages>  list=mesService.findmessages(sender,receiver);
            map.put("name",authService.getById(receiver));
            map.put("name2",authService.getById(sender));
            map.put("messagelist",list);
            templateEngine.render("chat.ftl",map,resp);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
