package servlet;

import classes.User;
import dao.UserDao;
import service.AuthService;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;

public class RegistrationServlet extends HttpServlet {
    private AuthService authService;

    public RegistrationServlet(AuthService authService) {
        this.authService = authService;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Path path = Paths.get("templates/registration.html");
        ServletOutputStream os = resp.getOutputStream();
        Files.copy(path, os);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User user=new User(req.getParameter("name"),req.getParameter("surname"),req.getParameter("email"),req.getParameter("password"),req.getParameter("occupation"),req.getParameter("image"));
        try {
            authService.save(user);
        } catch (SQLException e) {
            resp.sendRedirect("/registr/*");
        }

        resp.sendRedirect("/login/*");
    }
}
