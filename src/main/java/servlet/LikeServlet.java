package servlet;

import classes.Like;
import classes.User;
import service.AuthService;
import service.LikeService;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class LikeServlet extends HttpServlet {
   private TemplateEngine templateEngine;
   private LikeService likeService;
   private AuthService authService;

    public LikeServlet(TemplateEngine templateEngine, LikeService likeService, AuthService authService) {
        this.templateEngine = templateEngine;
        this.likeService = likeService;
        this.authService = authService;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //templateEngine.render("people-list.ftl",resp);
        int fromwho=-1;
        Cookie[] cookies=req.getCookies();
        if(cookies!=null){
            for(Cookie c:cookies){
                if(c.getName().equals("%likes%")) {
                    fromwho=Integer.parseInt(c.getValue());
                }
            }
        }
        if(fromwho!=-1){
            try {
                List<User> list=new ArrayList<>();
                List<Like> likeList=likeService.getById(fromwho);
                System.out.println(likeList);
                for (int i = 0; i <likeList.size() ; i++) {
                    User user=authService.getById(likeList.get(i).getToWho());
                    list.add(user);
                }
                HashMap<String,Object> data=new HashMap<>();
                data.put("peoplelist",list);
                templateEngine.render("people-list.ftl",data,resp);

            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
        else {
            resp.sendRedirect("/login");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int fromwho=-1;
        Cookie[] cookies=req.getCookies();
        if(cookies!=null){
            for(Cookie c:cookies){
                if(c.getName().equals("%likes%")) {
                    fromwho=Integer.parseInt(c.getValue());
                }
            }
        }
        if(fromwho!=-1){
            try {
                List<User> list=new ArrayList<>();
                List<Like> likeList=likeService.getById(fromwho);
                for (int i = 0; i <likeList.size() ; i++) {
                    User user=authService.getById(likeList.get(i).getToWho());
                    list.add(user);
                }
                HashMap<String,Object> data=new HashMap<>();
                data.put("peoplelist",list);
                templateEngine.render("people-list.ftl",data,resp);

            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
        else {
            resp.sendRedirect("/login");
        }
    }
}
