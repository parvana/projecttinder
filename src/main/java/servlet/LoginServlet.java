package servlet;
import dao.UserDao;
import service.AuthService;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;

public class LoginServlet extends HttpServlet {
    UserDao userDao=new UserDao();
    AuthService authService = new AuthService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Path path = Paths.get("templates/login.html");
        ServletOutputStream os = resp.getOutputStream();
        Files.copy(path, os);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String email=req.getParameter("email");
        String password=req.getParameter("password");

        try {
            if(authService.check(email,password)) {
                try {
                    int id = authService.getByEmail(email).getId();
                    Cookie c = new Cookie("%likes%", String.valueOf(id));
                    c.setPath("/");
                    resp.addCookie(c);
                    resp.sendRedirect("/users");

                } catch (SQLException e) {
                    System.out.println("there is not such mail");
                }
            }
            else{
                resp.sendRedirect("/login");
            }
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("check");
        }


    }
}
