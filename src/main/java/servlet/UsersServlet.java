package servlet;

import classes.Like;
import classes.User;
import dao.LikeDao;
import org.omg.PortableServer.LIFESPAN_POLICY_ID;
import service.AuthService;
import service.LikeService;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
public class UsersServlet extends HttpServlet {
    private TemplateEngine engine;
    private AuthService authService;
    private LikeService likeService;
    private int numberofusers;
    private    List<Integer> listid;
    int id=-1;
    public UsersServlet(TemplateEngine engine, AuthService authService, LikeService likeService) {
        this.engine = engine;
        this.authService = authService;
        this.likeService = likeService;

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        numberofusers=0;
        Cookie [] cookies=req.getCookies();
        if(cookies!=null){
            for(Cookie c:cookies){
                if(c.getName().equals("%likes%")) {
                    id=Integer.parseInt(c.getValue());
                }
            }
        }
        listid=new ArrayList<>();

            try {
                List<User> list = authService.getALL();
                for (int i = 0; i < list.size(); i++) {
                    if (list.get(i).getId() != id) {
                        if(likeService.beingLiked(id,list.get(i).getId())==false){
                            listid.add(list.get(i).getId());
                        }
                    }
                }
                HashMap<String, Object> data = new HashMap<>();
                if(numberofusers==listid.size()){
                    resp.sendRedirect("/like/*");
                }else {
                    data.put("id", authService.getById(listid.get(numberofusers)).getId());
                    data.put("name", authService.getById(listid.get(numberofusers)).getName());
                    data.put("image",authService.getById(listid.get(numberofusers)).getImageurl());
                    numberofusers++;
                    engine.render("like-page.html", data, resp);

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        String towho=req.getParameter("like");
        if(towho!=null) {
            try {
                Like like = new Like(id,Integer.parseInt(towho));
                likeService.save(like);
            } catch (SQLException e) {
                System.out.println("went wrong");
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       try {
           HashMap<String, Object> data = new HashMap<>();
           if(numberofusers==listid.size()){
               resp.sendRedirect("/like/*");
           }else {
               data.put("id", authService.getById(listid.get(numberofusers)).getId());
               data.put("name", authService.getById(listid.get(numberofusers)).getName());
               data.put("image",authService.getById(listid.get(numberofusers)).getImageurl());
               numberofusers++;
               engine.render("like-page.html", data, resp);
           }
       }catch (Exception e){
           e.printStackTrace();
       }
        String towho=req.getParameter("like");
        if(towho!=null) {
            try {
                Like like = new Like(id,Integer.parseInt(towho));
                likeService.save(like);
            } catch (SQLException e) {
                System.out.println("went wrong");
            }
        }

    }
}
