package classes;

public class Messages {
    private int id;
    private int sender;
    private int receiver;
    private String message;

    public Messages(int id, int sender, int receiver, String message) {
        this.id = id;
        this.sender = sender;
        this.receiver = receiver;
        this.message = message;
    }

    public Messages(int sender, int receiver, String message) {
        this.sender = sender;
        this.receiver = receiver;
        this.message = message;
    }

    public int getId() {
        return id;
    }

    public int getSender() {
        return sender;
    }

    public int getReceiver() {
        return receiver;
    }

    public String getMessage() {
        return message;
    }
}
