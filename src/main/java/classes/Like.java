package classes;

public class Like {
    private int id;
    private int fromWhom;
    private int toWho;

    public Like(int id, int fromWhom, int toWho) {
        this.id = id;
        this.fromWhom = fromWhom;
        this.toWho = toWho;
    }

    public Like(int fromWhom, int toWho) {
        this.fromWhom = fromWhom;
        this.toWho = toWho;
    }

    public int getId() {
        return id;
    }

    public int getFromWhom() {
        return fromWhom;
    }

    public int getToWho() {
        return toWho;
    }

    public void setFromWhom(int fromWhom) {
        this.fromWhom = fromWhom;
    }

    public void setToWho(int toWho) {
        this.toWho = toWho;
    }
}
