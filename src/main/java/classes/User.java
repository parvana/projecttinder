package classes;

public class User {
    private int id;
    private String name;
    private String surname;
    private String occupation;
    private String email;
    private String password;
    private String imageurl;


    public User(String name, String surname, String email, String password,String occupation,String imageurl) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.password = password;
        this.occupation=occupation;
        this.imageurl=imageurl;
    }

    public User(int id, String name, String surname, String email, String password,String occupation,String imageurl) {
        this.id=id;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.password = password;
        this.occupation=occupation;
        this.imageurl=imageurl;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getOccupation() {
        return occupation;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getImageurl() {
        return imageurl;
    }



    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }


}
